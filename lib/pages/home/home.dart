import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:myloginapp/constants/index.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _getDataForLocal;
  @override
  void initState() {
    super.initState();
    _getdata();
  }

  Future _getdata() async {
    var localStorage = await SharedPreferences.getInstance();
    var getdata = localStorage.getString(DATA_LOCALSTORAGE);
    print(jsonDecode(getdata.toString()));
    setState(() {
      _getDataForLocal = jsonDecode(getdata.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          brightness: Brightness.dark,
          toolbarHeight: 0,
        ),
        body: Column(
          children: [
            SizedBox(height: 20),
            Container(
              alignment: Alignment.center,
              child: Text(
                "${_getDataForLocal?["data"]?["note"] ?? ""}",
              ),
            ),
            SizedBox(height: 20),
            Container(
              alignment: Alignment.center,
              child: Text(
                "Name: ${_getDataForLocal?["data"]?["firstName"] ?? ""}",
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                "lastName: ${_getDataForLocal?["data"]?["lastName"] ?? ""}",
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                "Phone: ${_getDataForLocal?["data"]?["phone"] ?? ""}",
              ),
            )
          ],
        ));
  }
}
