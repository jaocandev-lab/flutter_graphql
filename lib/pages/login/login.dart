import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:myloginapp/apiService/login_service.dart';
import 'package:myloginapp/constants/index.dart';
import 'package:myloginapp/helper/schoolicon_icons.dart';
import 'package:myloginapp/pages/home/home.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  LoginService _loginService = LoginService();
  TextEditingController _phone = TextEditingController();
  TextEditingController _password = TextEditingController();
  get children => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
            child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                  top: 90.0, bottom: 20.0, left: 50.0, right: 50.0),
              child: Center(
                child: Container(
                  padding: EdgeInsets.only(top: 25.0, right: 5.0),
                  child: Column(
                    children: [
                      Icon(
                        Schoolicon.graduation,
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Text(
                          "JaoCanDev",
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    color: HexColor("#b5b5b5"),
                    borderRadius: BorderRadius.circular(65.0),
                  ),
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Column(children: [
                  Row(
                    children: [
                      Icon(
                        Icons.email,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 10.0),
                      ),
                      Text(
                        "Email",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  TextField(
                      controller: _phone,
                      decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey),
                          ),
                          // labelText: 'Email',
                          hintText: 'example@gmail.com',
                          hintStyle: TextStyle(color: Colors.grey))),
                ])),
            SizedBox(height: 15),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Column(children: [
                  Row(
                    children: [
                      Icon(
                        Icons.lock,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 10.0),
                      ),
                      Text(
                        "Password",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  TextField(
                    controller: _password,
                    obscureText: true,
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    ),
                  ),
                  SizedBox(height: 50)
                ])),
            Container(
              width: 300,
              height: 40,
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(HexColor("#014878")),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)))),
                child: const Text('Login'),
                onPressed: () async {
                  var _paramQL = {
                    "data": {
                      "phone": _phone.text.toString(),
                      "password": _password.text.toString()
                    }
                  };
                  var _result = await _loginService.login(_paramQL);
                  if (jsonEncode(_result.data?['login']) != 'null') {
                    var localStorage = await SharedPreferences.getInstance();
                    await localStorage.setString(
                        DATA_LOCALSTORAGE, jsonEncode(_result.data?['login']));
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  }
                },
              ),
            ),
          ],
        )),
      ),
    );
  }
}
