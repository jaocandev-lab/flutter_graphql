// ignore_for_file: file_names, unused_import, use_key_in_widget_constructors, prefer_const_constructors, sized_box_for_whitespace, avoid_unnecessary_containers, unnecessary_new, prefer_const_literals_to_create_immutables, camel_case_types, override_on_non_overriding_member, unused_local_variable, prefer_typing_uninitialized_variables, prefer_const_declarations, unnecessary_this, prefer_const_constructors_in_immutables, no_logic_in_create_state, must_be_immutable, prefer_final_fields, avoid_print, deprecated_member_use
// ignore: non_constant_identifier_names
final String LOGIN = r'''
mutation Mutation($data: UserAuthInput!) {
  login(data: $data) {
    accessToken
    refreshToken
    data {
      id
      firstName
      lastName
      phone
      createdAt
      updatedAt
      note
    }
  }
}

 ''';
