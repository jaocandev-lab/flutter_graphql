import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:myloginapp/apollo/login.dart';

import 'graphql_client.dart';

class LoginService {
  GraphQLClient _client = getGraphQLClient();

  LoginService() {
    _client = getGraphQLClient();
  }
  // create order
  Future<QueryResult> login(paramQL) async {
    final _options = MutationOptions(
        document: gql(LOGIN),
        variables: paramQL,
        fetchPolicy: FetchPolicy.networkOnly);
    final result = await _client.mutate(_options);
    return result;
  }
}
