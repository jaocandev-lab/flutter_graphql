import 'package:graphql/client.dart';
import 'package:myloginapp/constants/index.dart';
import 'package:shared_preferences/shared_preferences.dart';

final _httpLink = HttpLink(GRAPHQL_ENDPOINT);
final _authLink = AuthLink(
  getToken: () async {
    var localStorage = await SharedPreferences.getInstance();
    var getdata = localStorage.getString(DATA_LOCALSTORAGE);
    return getdata;
  },
);
Link _link = _authLink.concat(_httpLink);
GraphQLClient getGraphQLClient() {
  GraphQLClient _client = GraphQLClient(
    link: _link,
    cache: GraphQLCache(store: HiveStore()),
  );

  return _client;
}
