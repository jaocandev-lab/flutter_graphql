// ignore_for_file: unnecessary_null_comparison, use_key_in_widget_constructors

import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/material.dart';

String? uuidFromObject(Object object) {
  if (object is Map<String, Object>) {
    final typeName = object['__typename'];
    final String id = object['id'].toString();
    if (typeName != null && id != null) {
      return <dynamic>[typeName, id].join('/');
    }
  }
  return null;
}

ValueNotifier<GraphQLClient> clientFor({
  @required String? uri,
  String? subscriptionUri,
}) {
  Link link = HttpLink(uri!);
  if (subscriptionUri != null) {
    final WebSocketLink websocketLink = WebSocketLink(
      subscriptionUri,
    );

    link = Link.split((request) => request.isSubscription, websocketLink, link);
  }

  return ValueNotifier<GraphQLClient>(
    GraphQLClient(
      cache: GraphQLCache(store: HiveStore()),
      link: link,
    ),
  );
}

class ClientProvider extends StatelessWidget {
  ClientProvider({
    required this.child,
    required String uri,
    String? subscriptionUri,
  }) : client = clientFor(
          uri: uri,
          subscriptionUri: subscriptionUri,
        );

  final Widget child;
  final ValueNotifier<GraphQLClient> client;

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: client,
      child: child,
    );
  }
}
