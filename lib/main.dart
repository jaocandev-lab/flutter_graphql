// ignore_for_file: file_names, unused_import, use_key_in_widget_constructors, prefer_const_constructors, sized_box_for_whitespace, avoid_unnecessary_containers, unnecessary_new, prefer_const_literals_to_create_immutables, camel_case_types, override_on_non_overriding_member, unused_local_variable, prefer_typing_uninitialized_variables, prefer_const_declarations, unnecessary_this, prefer_const_constructors_in_immutables, no_logic_in_create_state, must_be_immutable, prefer_final_fields, avoid_print, deprecated_member_use, unused_element
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:myloginapp/constants/index.dart';
import 'package:myloginapp/pages/home/home.dart';
import 'package:myloginapp/pages/login/login.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'providers/client_provider.dart';

void main() async {
  await initHiveForFlutter();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var userData;
  @override
  void initState() {
    super.initState();
    checkData();
  }

  checkData() async {
    try {
      var localStorage = await SharedPreferences.getInstance();
      String? getdata = localStorage.getString(DATA_LOCALSTORAGE);
      if (getdata != null) {
        var _getdata = jsonDecode(getdata);
        setState(() {
          userData = _getdata;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClientProvider(
        uri: GRAPHQL_ENDPOINT,
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            scaffoldBackgroundColor: HexColor("001224"),
            primaryTextTheme: Typography(platform: TargetPlatform.iOS).white,
            textTheme: Typography(platform: TargetPlatform.iOS).white,
          ),
          home: userData != "null" ? Login() : Home(),
        ));
  }
}
